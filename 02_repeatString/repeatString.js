const repeatString = function(string, num) {
    let nuevaCadena = '';

    if (num < 0){
        return "ERROR"
    }
    
    for (i=0; i<num; i++){
        nuevaCadena += string
    }
    return nuevaCadena;
};

// Do not edit below this line
module.exports = repeatString;
