# Ejercicio 01 - Hola Mundo

El propósito principal de este ejercicio es guiarte a través del proceso de ejecutar las pruebas y asegurarte de que todo esté configurado y funcionando correctamente.

En este directorio encontrarás otros 2 archivos:
  1. `helloWorld.js`
  2. `helloWorld.spec.js`

Esta configuración debería ser la misma para todos los ejercicios. El archivo de JavaScript simple es donde escribirás tu código, y el archivo `spec` contiene las pruebas que verifican que tu código sea funcional.

Comencemos por mirar el archivo de especificaciones (`spec`):
```javascript
const helloWorld = require('./helloWorld');

describe('Hola Mundo', function() {
  test('dice "¡Hola, Mundo!"', function() {
    expect(helloWorld()).toEqual('¡Hola, Mundo!');
  });
});

```

En la parte superior del archivo, usamos `require()` para importar el código del archivo JavaScript (`helloWorld.js`) para que podamos probarlo.

El siguiente bloque (`describe()`) es el cuerpo de la prueba. Básicamente, lo que hace es ejecutar tu código y probar si la salida es correcta. La función `test()` describe lo que debería estar sucediendo en inglés simple y luego incluye la función `expect()`. Para este ejemplo simple, debería ser bastante fácil de leer.

Por ahora, no necesitas preocuparte por cómo escribir pruebas, pero debes intentar familiarizarte lo suficiente con la sintaxis como para entender qué te piden las pruebas. Ve y ejecuta las pruebas ingresando `npm test helloWorld.spec.js` en la terminal y observa cómo falla. La salida de ese comando debería decirte exactamente qué salió mal con tu código. En este caso, al ejecutar la función `helloWorld()` debería devolver la frase 'Hello, World!', pero en su lugar devuelve una cadena vacía...

Ahora veamos el archivo JavaScript:
```javascript
const helloWorld = function() {
  return ''
}

module.exports = helloWorld
```
En este archivo, tenemos una función simple llamada helloWorld que devuelve una cadena vacía... justo lo que nuestra prueba estaba señalando. El `module.exports` en la última línea es cómo exportamos la función para que pueda ser importada con `require()` en el archivo de especificación.

Ve a ver si puedes hacer que la prueba pase editando el valor de retorno de la función y luego ejecutando nuevamente el archivo de prueba.

Solo para asegurarnos, en caso de que estés confundido en este punto, la prueba te está diciendo que ejecutar la función `helloWorld` debería devolver la frase `Hello, World!`. La puntuación y la capitalización son importantes aquí, así que verifica eso si la prueba aún no está pasando.

Así es como debería verse la función final:
```javascript
const helloWorld = function() {
  return 'Hello, World!'
}

module.exports = helloWorld
```

En su mayor parte, hemos configurado estas pruebas de manera que solo tienes que escribir el código que está siendo probado. No deberías preocuparte por importar o exportar nada en esta etapa... así que solo trabaja en esa parte del código y escribe lo necesario para que pasen las pruebas.
