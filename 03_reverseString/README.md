# Ejercicio 03 - Invertir una Cadena

Muy simple, escribe una función llamada `reverseString` que devuelva su entrada, invertida.

```javascript
reverseString('hola mundo') // devuelve 'odnum aloh'
```

Observarás en este ejercicio que hay múltiples pruebas. Después de hacer que la primera pase, habilita las demás una por una eliminando el `.skip` frente a la función `test.skip()`.

## Pistas
Las cadenas en JavaScript no pueden invertirse directamente, así que tendrás que dividirla en algo diferente primero, realizar la inversión y luego unirla nuevamente en una cadena.