const removeFromArray = function(array, ...numbers) {
    let nuevaCadena = [];

    for (i=0; i < array.length; i++){
        let encontrado = false;

        for (j=0; j < numbers.length; j++){
            if (array[i] === numbers[j]) {
                encontrado = true;
                break;
            }
        }
            
        if (!encontrado){
            nuevaCadena.push(array[i]);
        }
    }
    
    return nuevaCadena;
}

// Do not edit below this line
module.exports = removeFromArray;
