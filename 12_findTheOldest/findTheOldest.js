var findTheOldest = function(personas) {
    var personaMasAnciana = personas.reduce(function(personaMasVieja, personaActual) {
        var edadMasVieja = obtenerEdad(personaMasVieja);
        var edadActual = obtenerEdad(personaActual);
        
        if (edadMasVieja > edadActual) {
            return personaMasVieja;
        } else {
            return personaActual;
        }
    });

    return personaMasAnciana;
};

function obtenerEdad(persona) {
    var añoActual = new Date().getFullYear();

    if (!persona.yearOfDeath) {
        return añoActual - persona.yearOfBirth;
    } else {
        return persona.yearOfDeath - persona.yearOfBirth;
    }
}

// Do not edit below this line
module.exports = findTheOldest;
