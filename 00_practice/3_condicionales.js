// Funciones
function identificarNumero(num){
    if (num >0){
        return "El número es positivo";
    }

    else if (num < 0 ){
        return "El número es negativo";
    }

    else{
        return "El número es cero";
    }

    

    
}

// Ejemplos de uso
console.log(identificarNumero(5)); // Debería imprimir "El número es positivo"
console.log(identificarNumero(-3)); // Debería imprimir "El número es negativo"
console.log(identificarNumero(0)); // Debería imprimir "El número es cero"

// Do not edit below this line
module.exports = { identificarNumero };
