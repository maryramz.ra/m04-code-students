    // Funciones
    function sumar(num1,num2){
        return num1 + num2;
    }

    function restar(num1, num2){
        return num1 - num2;
    }

    function multiplicar(num1, num2){
        return num1*num2;
    }

    function dividir(num1, num2){
        if (num2 == 0){
            return "No se puede dividir por cero."
        }
        return num1/num2;
    }

    // Ejemplos de uso
    console.log('Suma:');
    console.log(sumar(10,5))
    console.log('Resta:');
    console.log(restar(10,5))
    console.log('Multiplicación:');
    console.log(multiplicar(10,5))
    console.log('División:');
    console.log(dividir(10,0))

    // Do not edit below this line
    module.exports = { sumar, restar, multiplicar, dividir };
