//Funciones 
function esPalindromo(palabra) {
    palabra = palabra.toLowerCase();
    var longitud = palabra.length;
    var i = 0;
    var j = longitud - 1;
    while (i < j) {
        if (palabra[i] !== palabra[j]) {
            return false;
        }
        i++;
        j--;
    }
    return true;
}

function sumaNumerosPares(numero) {
    var suma = 0;
    for (var i = 2; i <= numero; i += 2) {
        suma += i;
    }
    return suma;
}


// Ejemplos de uso
console.log(esPalindromo("reconocer")); // Debería imprimir true
console.log(esPalindromo("javascript")); // Debería imprimir false

console.log(sumaNumerosPares(8)); // Debería imprimir 20 (2 + 4 + 6 + 8)
console.log(sumaNumerosPares(10)); // Debería imprimir 30 (2 + 4 + 6 + 8 + 10)

// Do not edit below this line
module.exports = { esPalindromo, sumaNumerosPares };
