// Funciones
function encontrarMaximo(arr) {
    if (arr.length == 0) return;
    let max = arr[0];
    for (let i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
}

function invertirArray(arr) {
    let Arrayinvertido = [];
    for (let i = arr.length - 1; i >= 0; i--) {
        Arrayinvertido.push(arr[i]);
    }
    return Arrayinvertido;
}

function calcularSuma(arr) {
    let suma = 0;
    for (let i = 0; i < arr.length; i++) {
        suma += arr[i];
    }
    return suma;
}

// Ejemplo de uso
console.log(encontrarMaximo([3, 7, 1, 10, 5])); // Debería imprimir 10
console.log(encontrarMaximo([-2, -8, -1, -5])); // Debería imprimir -1

console.log(invertirArray([1, 2, 3, 4])); // Debería imprimir [4, 3, 2, 1]
console.log(invertirArray(['a', 'b', 'c'])); // Debería imprimir ['c', 'b', 'a']

console.log(calcularSuma([1, 2, 3, 4])); // Debería imprimir 10
console.log(calcularSuma([-5, 10, 2])); // Debería imprimir 7


// Do not edit below this line
module.exports = { encontrarMaximo, invertirArray, calcularSuma };
