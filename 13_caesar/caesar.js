const caesar = function(cadena, desplazamiento) {
    let resultado = '';

    for (let i = 0; i < cadena.length; i++) {
        let caracter = cadena[i];
        let codigo = cadena.charCodeAt(i);

        if (codigo >= 65 && codigo <= 90 || codigo >=97 && codigo <= 122 ) { 
            let nuevaLetra = codigo + desplazamiento
            console.log(nuevaLetra)
            let letra = String.fromCharCode(nuevaLetra)

            resultado += letra
        }
        
        else{
            let letra = String.fromCharCode(codigo)
            resultado += letra
        }       
    }

    return resultado;
};

// Do not edit below this line
module.exports = caesar;
