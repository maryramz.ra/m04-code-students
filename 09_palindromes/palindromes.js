const palindromes = function (cadena) {
    cadena = cadena.toLowerCase().replace(/[^a-z0-9]/g, '');
    var esPalindromo = true;

    for (let i = 0; i < cadena.length / 2; i++) {
        if (cadena[i] !== cadena[cadena.length - 1 - i]) {
            esPalindromo = false;
            break;
        }
    }
    return esPalindromo;
};

// Do not edit below this line
module.exports = palindromes;
