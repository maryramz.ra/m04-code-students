const fibonacci = function(numero) {
    if (numero < 0){
        return "OOPS";
    }

    let numeroAnterior = 0;
    let numeroActual = 1;

    for (let i = 2; i <= numero; i++) {
        let siguienteNumero = numeroAnterior + numeroActual;
        numeroAnterior = numeroActual;
        numeroActual = siguienteNumero;
    }

    return numeroActual;
};

// Do not edit below this line
module.exports = fibonacci;
