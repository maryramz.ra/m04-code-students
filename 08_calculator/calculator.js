const add = function(num1, num2) {
	return num1 + num2
};

const subtract = function(num1, num2) {
	return num1 - num2
};

const sum = function(array) {
  let totalsuma=0;
	for (i =0; i <array.length; i++ ){
    totalsuma += array[i]
  }
  return totalsuma
};

const multiply = function(array) {
  let totalMulti= 1;
  for (i=0; i <array.length; i++){
    totalMulti *= array[i]
  }
  return totalMulti
};

const power = function(num1, num2) {
	return Math.pow(num1, num2)
};

const factorial = function(num) {
  let factorial = 1;
	
  for (i =2; i <= num; i++){
    factorial = factorial * i;
  }

  return factorial
};

// Do not edit below this line
module.exports = {
  add,
  subtract,
  sum,
  multiply,
  power,
  factorial
};
