Este ejercicio es complicado y fue eliminado de nuestras recomendaciones porque en su mayoría utiliza expresiones regulares para la solución, y esas no se enseñan realmente en este punto de nuestro plan de estudios.

Lo dejamos aquí para la posteridad, o como un buen desafío para cualquiera que quiera intentarlo.

# Ejercicio XX - snakeCase

Convierte frases y palabras a snake case

> Snake case (o snake_case) es la práctica de escribir palabras compuestas o frases en las cuales los elementos están separados con un único carácter de guion bajo (\_) y sin espacios, con la letra inicial de cada elemento generalmente en minúscula, como en "foo_bar".


```javascript
snakeCase('Hello, World!') // hello_world
snakeCase('snakeCase') // snake_case
```
